var postman = {};
$(function () {
    'use strict';
    postman = {

        serializer: null,
        requestconstructor: null,
        requesthandler: null,
        feedback: null,
        formhandler: null,
        init: function () {
            this.serializer = new postman.Serializer();
            this.requestconstructor = new postman.RequestConstructor();
            this.requesthandler = new postman.RequestHandler();
            this.feedback = new postman.Feedback();
            this.formhandler = new postman.FormHandler();
        },

        Serializer: function () {
            var form = $('form'),
                me = this;

            function construct(){
                var datas = $('.post-data li.active input');

                for(var i = 0; i < datas.length; i += 2){
                    me.body = [];
                    me.string = '{"'+datas[i].value+'":"'+datas[i+1].value+'"}';
                    me.body.push(me.string);
                }

                //@todo currently only returns the last key-value pair stored
                //@todo store the array elsewhere and write to it before returning the new result
                return ($.extend({}, me.body)[0]);
            }

            form.on('submit', function(e){
                e.preventDefault();
                var url = $('form .url').val();
                var body = construct();

                if(url.length){
                    postman.RequestConstructor(url, body);
                }

            });

        },

        RequestConstructor: function (url, data) {
            //@todo the number entered in the form is currently unreliable.
            //if an error gets thrown the $.ajax call loop tends to skip some iterations
            //add support for get requests

            var number = $('.repeat').val();

            console.time('request dispatcher bmark');

            for(var i = 0; i < number; i++){

                $.ajax({
                    type : 'post',
                    url : url,
                    data : $.parseJSON(data),
                    success : function(success_data){
                        console.log(success_data);
                        postman.RequestHandler(success_data);
                    },
                    fail : function(failure_data){
                        console.log(failure_data);
                        postman.RequestHandler(failure_data);
                    }
                });

            }

            console.timeEnd('request dispatcher bmark');

        },

        RequestHandler: function (data) {
            //handle the response
            //dispatch appropriate data to postman.Feedback()
            //display errors if needed
            var me = this;

            postman.Feedback(data);

        },

        Feedback: function (data) {
            var feedback = $('.returned-data textarea');
            feedback.text(data);
        },

        FormHandler: function () {
            var dom = '<li class="inactive"><span class="key"><input type="text"/></span><span class="value"><input type="text"/></span></li>';

            $('form').on('focus', 'li.inactive', function(){
                $(this).removeClass('inactive').addClass('active');
                $(dom).insertAfter(this);
            });

        }

    };

    postman.init();

});